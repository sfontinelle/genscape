Bowling
by Steven Fontinelle

To Run:
pip install jsonpickle
python manage.py runserver

To play:
Create a match:
  Post a list of player names in format "first_name last_name" or to use an existing player, use their player_id.
The two can be intermixed

POST match/
e.g. POST localhost:8000/match/
{
"players" : ["player one", 37, "player three"]
}

You will get a list of Games as output with one Game corresponding to each Player

To Bowl:
POST game/<game_id>/ball/<pins_knocked_down>/
e.g. POST localhost:8000/game/51/ball/4/
no payload

You will get a list of frames as output. Add the scores of frames 1-10 to get the total score for the game. There is no
protection against creating more than 10 frames and the 11th frame is used for bonus balls. Frames with strikes or spares
will have a score of 0 until the next 2 or 1 balls respectively have been thrown.

