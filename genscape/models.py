from django.db import models

# Create your models here.


class Player(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)


class Match(models.Model):

    class Meta:
        db_table = "genscape_matches"


class Game(models.Model):
    player = models.ForeignKey(Player, null=True)
    match = models.ForeignKey(Match, null=True)
    player_order = models.IntegerField()


class Frame(models.Model):
    game = models.ForeignKey(Game, null=True)
    number = models.IntegerField()
    ball1 = models.IntegerField(null=True)
    ball2 = models.IntegerField(null=True)
    score = models.IntegerField(default=0)
    is_strike = models.BooleanField(default=False)
    is_spare = models.BooleanField(default=False)




















