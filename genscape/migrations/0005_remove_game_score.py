# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-04-09 06:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('genscape', '0004_auto_20170409_0623'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='game',
            name='score',
        ),
    ]
