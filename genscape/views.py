from django.http import HttpResponse
import json
import genscape.models as models
import jsonpickle


# POST match/
'''
input:
{
   "players" : ["player one", "first last", player_id]
}

output: a list of games
'''
def create_match(request):
    json_match = _load_json(request)
    json_players = json_match["players"]
    players = []
    for player_id in json_players:
        if isinstance(player_id, int) or player_id.isdigit():
            player = get_player(player_id)
        else:
            player = create_player(player_id)
        players.append(player)

    match_model = models.Match()
    match_model.save()

    games = []
    player_order = 1
    for player in players:
        game = models.Game()
        game.player = player
        game.match = match_model
        game.player_order = player_order
        player_order += 1
        game.save()
        games.append(game)

    # covert to python objects so we can serialize to json nicely
    match = Match()
    match.id = match_model.id

    for game_model in games:
        game = Game()
        game.id = game_model.id
        game.match_id = game_model.match.id
        game.player_id = game_model.player.id
        game.player_order = game_model.player_order
        match.games.append(game)

    match_json = jsonpickle.encode(match, unpicklable=False)

    return HttpResponse(match_json)


# POST game/<game_id>/ball/<pins>/
# return a list of frames
def throw_ball(request, game_id, pins):
    pins = int(pins)
    frames = models.Frame.objects.filter(game_id=game_id).order_by("number")
    frames = list(frames)

    # add the pins to the 2nd ball in the last frame or the first ball of a new frame
    last_frame = None
    if len(frames) == 0:
        pass
    else:
        index = len(frames) - 1
        if index < 0:
            index = 0
        last_frame = frames[index]

    if last_frame is not None and last_frame.ball2 is None and not last_frame.is_strike:
        last_frame.ball2 = pins
        print(last_frame.ball1)
        if last_frame.ball1 + last_frame.ball2 == 10:
            last_frame.is_spare = True
        last_frame.save()
    else:
        frame = models.Frame()
        frame.game_id = game_id
        frame.number = len(frames) + 1
        frame.ball1 = pins
        if pins == 10:
            frame.is_strike = True
        frame.save()
        frames.append(frame)

    # calculate new score
    # at most only 3 frames we need to score (the current frame and last 2 frames)
    i = len(frames)-3
    if i < 0:
        i = 0

    while i < len(frames):
        frame = frames[i]
        score = 0
        if frame.is_strike:
            if i+1 < len(frames):
                next_frame = frames[i+1]
                if next_frame.is_strike:
                    if i+2 < len(frames):
                        next_frame = frames[i+2]
                        score = 10 + 10 + next_frame.ball1
                else:
                    score = 10 + next_frame.ball1 + (next_frame.ball2 or 0)
        elif frame.is_spare:
            if i+1 < len(frames):
                next_frame = frames[i+1]
                score = 10 + next_frame.ball1
        else:
            score = frame.ball1 + (frame.ball2 or 0)
        frame.score = score
        frame.save()
        i += 1

    # covert to python objects so we can serialize to json nicely
    frames_object = []
    for frame_model in frames:
        frame = Frame()
        frame.game_id = frame_model.game_id
        frame.number = frame_model.number
        frame.ball1 = frame_model.ball1
        frame.ball2 = frame_model.ball2
        frame.score = frame_model.score
        frame.is_strike = frame_model.is_strike
        frame.is_spare = frame_model.is_spare
        frames_object.append(frame)

    frames_json = jsonpickle.encode(frames_object, unpicklable=False)
    return HttpResponse(frames_json)


def get_player(player_id):
    player = models.Player.objects.get(id=player_id)

    return player


def create_player(name):
    name = name.split(" ")

    player = models.Player()
    player.first_name = name[0]
    player.last_name = name[1]
    player.save()

    return player


def _load_json(request):
    return json.loads(request.body.decode("utf-8"))


class Match(object):

    def __init__(self):
        self.id = None
        self.games = []


class Game(object):

    def __init__(self):
        self.id = None
        self.player_id = None
        self.match_id = None
        self.player_order = None
        self.frames = []


class Frame(object):

    def __init__(self):
        self.game_id = None
        self.number = None
        self.ball1 = None
        self.ball2 = None
        self.score = 0
        self.is_strike = False
        self.is_spare = False
